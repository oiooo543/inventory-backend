<?php

namespace App\Http\Controllers;


use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    protected $product;

    public function setUp():void
    {

        $this->product = [
            'name' => 'Sally',
            'price' => 10.00,
            'qty' => 10
        ];

        parent::setUp();
    }

    public function testStore()
    {
        $response = $this->json('POST', '/api/v1/product', $this->product);

        $response->assertStatus(201);
    }

    public function testScoreIncompeteDetails(){
        $response = $this->json('POST', '/api/v1/product',
            [
                'name' => 'Sally',
                'price' => 10.00,

            ]);

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => true,
            ]);
    }

    public function testIndex()
    {
        $response = $this->json('GET', '/api/v1/product');

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => true,
            ]);
    }

    public function testUpdate()
    {
        $response = $this->json('PATCH', 'api/v1/product/1', [
            'price' => 30
        ]);

        $response
            ->assertStatus(200);
            $this->assertNotTrue($response->json() == $this->product);



    }

    public function testShow()
    {
        $response = $this->json('GET', 'api/v1/product/1');

        $response
            ->assertStatus(200)
            ->assertJsonCount(1);
    }
}
